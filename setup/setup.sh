cd ~

echo APT Update
sudo -y apt update
sudo -y apt-get update
sudo -y apt-get upgrade

echo Dashboard Folder
mkdir ~/temp
chmod +x ~/dashboard/autorun.sh

echo Scripts Folder
chmod +x ~/scripts/compose.sh

echo Visual Studio Code Server
cd ~/temp
wget https://github.com/cdr/code-server/releases/download/v3.12.0/code-server_3.12.0_amd64.deb
sudo dpkg -i code-server_3.12.0_amd64.deb
systemctl --user start code-server
systemctl --user enable code-server
rm sudo code-server_3.12.0_amd64.deb

cd ~
echo Install Docker
sudo apt-get -y remove docker docker-engine docker.io containerd runc
sudo apt-get -y remove docker-ce docker-ce-cli containerd.io
sudo apt-get -y autoremove
sudo apt-get -y clear
sudo apt-get -y update
sudo apt-get -y install ca-certificates curl gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker ${USER}
su - ${USER}

echo Install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo Pull Docker Images

docker pull python
docker pull node

docker pull mysql
docker pull mongo
docker pull redis

docker pull ubuntu

docker pull docker

docker pull scrapinghub/splash

echo Complit Setup