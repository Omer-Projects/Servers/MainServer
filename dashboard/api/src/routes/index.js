var express = require('express');
var router = express.Router();

/* GET home page. */
router.all('/', function(req, res, next) {
	res.redirect('/help');
});

router.all('/help', function(req, res, next) {
	res.json({
		'command': 'help',
		'commands': [
			{
				'name': 'help',
				'description': 'Return list of all the commands'
			}
		]
	  });
  });

module.exports = router;
