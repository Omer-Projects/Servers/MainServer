'use strict';

const express = require('express');
const path = require('path');

const indexRouter = require('./routes/index')

const app = express();

// Settings
const port = 80;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.listen(port, () => {
	console.log('Open the port ' + port);
});